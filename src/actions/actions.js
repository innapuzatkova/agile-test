import { FETCH_SYNONYMS, FETCH_SYNONYMS_SUCCESS } from '../constants/action-types';

export const fetchSynonyms = word => ({
  type: FETCH_SYNONYMS,
  word,
});

export const fetchSynonymsSuccess = result => {
  return {
    type: FETCH_SYNONYMS_SUCCESS,
    result,
  };
};
