import React from 'react';
import { combineReducers } from 'redux';
import { FETCH_SYNONYMS, FETCH_SYNONYMS_SUCCESS } from '../constants/action-types';

export const synonyms = (state = {}, action) => {
  switch (action.type) {
    case FETCH_SYNONYMS_SUCCESS:
      return { ...state, ...action.result };
    case FETCH_SYNONYMS:
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  synonyms,
});
