import { put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_SYNONYMS, FETCH_SYNONYMS_SUCCESS } from '../constants/action-types';
import { fetchSynonymsSuccess } from '../actions/actions';

function* fetchSynonymsSaga({ word }) {
  try {
    const synonyms = yield fetch(`https://api.datamuse.com/words?rel_syn=${word}`).then(result => {
      return result.json();
    });

    const resultWordPair = { [word]: synonyms };
    yield put(fetchSynonymsSuccess(resultWordPair));
    // yield put({ type: FETCH_SYNONYMS_SUCCESS, resultWordPair });
    // yield put(FETCH_SYNONYMS_SUCCESS);
  } catch (err) {
    console.log(err);
  }
}

export default function* rootSaga() {
  yield all([takeEvery(FETCH_SYNONYMS, fetchSynonymsSaga)]);
}
