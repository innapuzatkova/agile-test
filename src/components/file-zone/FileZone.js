import React, { Component } from 'react';
import './FileZone.css';

class FileZone extends Component {
  render() {
    return (
      <div id="file-zone">
        <div id="file" onMouseUp={this.props.getSynonyms} onDoubleClick={this.props.getSynonyms} contentEditable></div>
      </div>
    );
  }
}

export default FileZone;
