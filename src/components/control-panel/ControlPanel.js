import React, { Component } from 'react';
import './ControlPanel.css';

class ControlPanel extends Component {
  formatTextBold = () => {
    document.execCommand('bold');
  };
  formatTextItalic = () => {
    document.execCommand('italic');
  };
  formatTextUnderline = () => {
    document.execCommand('underline');
  };
  //possible improvements: make Button component which will contain
  // state of itself (state = {clicked: boolean}) and receive callback handler
  render() {
    return (
      <div id="control-panel">
        <div id="format-actions">
          <button className="format-action" type="button" onClick={this.formatTextBold}>
            <b>B</b>
          </button>
          <button className="format-action" type="button" onClick={this.formatTextItalic}>
            <i>I</i>
          </button>
          <button className="format-action" type="button" onClick={this.formatTextUnderline}>
            <u>U</u>
          </button>
        </div>
      </div>
    );
  }
}

export default ControlPanel;
