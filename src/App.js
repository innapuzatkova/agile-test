import React, { Component } from 'react';
import './App.css';
import ControlPanel from './components/control-panel/ControlPanel';
import FileZone from './components/file-zone/FileZone';
import getMockText from './text.service';
import { connect } from 'react-redux';
import { fetchSynonyms } from './actions/actions';

class App extends Component {
  state = {
    selectedWord: '',
  };
  getSynonymsHandler = () => {
    const selectedWord = window.getSelection().toString();
    if (!!selectedWord) {
      this.props.getSynonyms(selectedWord);
      this.setState({ selectedWord });
    }
  };
  changeWord = e => {};
  insertText;
  render() {
    const synonymsList = this.props.synonyms[this.state.selectedWord];
    return (
      <div className="App">
        <header>
          <span>Simple Text Editor</span>
        </header>
        <main>
          <ControlPanel />
          <FileZone getSynonyms={this.getSynonymsHandler} />
        </main>

        <ul>
          {synonymsList &&
            synonymsList.map((item, index) => {
              return (
                <li key={index} onClick={this.changeWord}>
                  {item.word}
                </li>
              );
            })}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = ({ synonyms }) => ({ synonyms });

export default connect(mapStateToProps, { getSynonyms: fetchSynonyms })(App);
